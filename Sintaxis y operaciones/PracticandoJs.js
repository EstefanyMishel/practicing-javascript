// Esto es un comentario de fila

/**
 * Esto es un comentario de bloque que es mas practico
 * para el uso de comentarios, el comentario de fila es mas recomendable usarlo para codigo
 * que no se desea ver o hacer correr.
 */

console.log('Hola Mundo');
console.log('Me llamo Estefany Mishel Paco Andrade');

/**
 * TIPOS DE DATOS
 */

    // Numbers-Para todos los tipos de numeros: enteros, decimales, negativos y positivos
console.log(25.6);
console.log(typeof 6);

    // String-Cadena de textos, espacios
console.log('Estefany preciosa');
console.log(typeof'Estefany preciosa');

    // Boolean-True o Falso
console.log(typeof false);
console.log(typeof true);
console.log(false);

/**
 * COLECCION DE DATOS
 */
    // ARRAY (Arreglo)
    /**Los objetos son colecciones de datos que tienen un identificador y un valor
     * el identificador es su posicion y el valor es lo que se encuentra entre comillas o el dato numerico
     */
console.log(typeof ['Jose', 'Juan','Jean', 'Oto']);
console.log(['Jose', 'Juan','Jean', 'Oto']);
console.log([1, 2, 3])

    // Objeto en JavaScript
console.log(typeof {
    nombre: 'Estefany',
    apellido: 'Paco',
});
console.log({
    nombre: 'Estefany',
    apellido: 'Paco',
});

    // Nulo-null
console.log(typeof null);
console.log(null);

/** 
 * NO DEFINIDO-UNDEFINED
 */
console.log(typeof undefined);

/**
 * VARIABLES
 */
    // Declaracion
let alumno
    // Asignacion
alumno = 'Estefany'
console.log(alumno)
    // Declaracion y asignacion
let alumnos = 'Estefany Mishel'
console.log(alumnos)
    // Reasignacion
alumnos = 1
console.log(alumnos)

/**
 * ASIGNACION POR REFERENCIA
 */
let profesor = 'Tefy'
let estudiante = profesor
console.log(profesor)
console.log(estudiante)

/** 
 *  CONSTANTES
 * Solo se permite la declaracion y asignacion y no asi reasignaciones
 * Se escriben generalmente en mayusculas y cada palabra separada con barras bajas
 */
const NOMBRE_EMPRESA = 'EDTeam'
console.log(NOMBRE_EMPRESA)

/**
 * OPERADORES ARITMETICOS
 */
    // Suma, resta, multiplicacion y residuo de la division (+, -, *, /, %)
let suma = 50+60
console.log(suma)

let resta= 50-60
console.log(resta)

let multip = 50*60
console.log(multip)

let dividi = 50/60
console.log(dividi)

let residuo = 60 % 4
console.log(residuo)

let numero1 = 55
let numero2 = 20
console.log(numero1*numero2)

    // Se puede sumar, restar, dividir y multiplicar directamente haciendo reasignaciones
let a = 10
let b = 4
a += b
console.log(a)
b -= a
console.log(b)
a /= b
console.log(a)
a *= b
console.log(a)
a %= b
console.log(a)

    // Exponenciales, tambien se puede realizar reasignaciones
let c = 5
let d = 3
console.log(c**d)

c **= d
console.log(c)

    // Operaciones de concatenacion
let miNombre = 'Marcela';
let miApellido = 'Flores';
console.log(miNombre + ' ' + miApellido);

    // Template string - con back string
let nameComplete = `Mi nombre es ${miNombre} y mi apellido es ${miApellido}`
console.log(nameComplete)

    // Operadores de comparacion:
    // Es igual == (compara solo el valor del dato)
let e = 50
let f = '50'
console.log(e == f)
    // Es estrictamente igual === (compara el valor del dato y el tipo de dato)
console.log(e === f)
    // Diferencia != (verifica que los valores de los datos sean distintos)
console.log(e != f)
    // Estrictamente diferente !== (verifica que los valores de datos sean distintos y el tipo de dato)
console.log(e !== f)

    // Operadores Logicos
    // And (&&) y or (||)
console.log(a > b && a < c)
console.log(a === b || b !== c)

    // Operadores Unarios
let g = 6
console.log(g++)
    // Operadores Ternarios
let h = 2
console.log (i = g > h ? 'Es verdad': 'Es falso')

    // Type Coercion
let j = '22'
let resp = h + j
console.log(typeof resp);



















